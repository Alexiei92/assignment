/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm;

/**
 *
 * @author Alessio
 */
class CifrarioOTP implements Cifrario {

    @Override
    public byte[] cifra(byte[] msg, byte[] key) {
        byte[] out = new byte[msg.length];
        int xor;
        for (int i = 0; i < out.length; i++) {
            xor = ((int) msg[i]) ^ ((int) key[i % key.length]);
            out[i] = (byte) (0xff & xor);
        }
        return out;
    }

    @Override
    public byte[] decifra(byte[] msg, byte[] key) {
        return cifra(msg, key);
    }
}
