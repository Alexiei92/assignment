/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm;

/**
 *
 * @author Alessio
 */
public class CifrarioFactory {

    public enum Algoritmo {

        OTP, AES_128, DES_56, DES_168
    }

    public static Cifrario getCifrario(Algoritmo alg) {
        Cifrario c;
        switch (alg) {
            case OTP:
                c = new CifrarioOTP();
                break;
            case AES_128:
                c = new CifrarioAES128();
                break;
            case DES_56:
                c = new CifrarioDES();
                break;
            case DES_168:
                c = new CifrarioDESTriplo();
                break;
            default:
                c = new CifrarioVuoto();
        }
        return c;
    }
}
