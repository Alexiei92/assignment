/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm;

/**
 *
 * @author Alessio
 */
public interface Cifrario {

    /**
     * 
     * @param msg messaggio da cifrare
     * @param key chiave di cifratura
     * @return messaggio cifrato
     */
    public byte[] cifra(byte[] msg, byte[] key);

    /**
     * 
     * @param msg messaggio da decifrare
     * @param key chiave di decifratura
     * @return messaggio decifrato
     */
    public byte[] decifra(byte[] msg, byte[] key);
}
